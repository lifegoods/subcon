<?php
function connect(){
    $server = "localhost"; //127.0.0.1
    $dbname = "sub_con";
    $user = "root";
    $password = "";
    $option = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO:: ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false, 
    ];

    try{
        return new PDO("mysql:host=$server;dbname=$dbname", $user, $password, $option);
    } catch ( \PDOException $e) {
        throw new PDOException($e->getMessage());
 }
}