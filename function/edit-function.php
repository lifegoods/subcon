<?php

if(isset($_GET['blog_id'])){
    $id = verifyInput($_GET['blog_id']);

}

$titreError = $articleError = "";

if(isset($_POST['modifier'])){
    udpate_article($_POST);
}

function udpate_article($data){

    $titre      = verifyInput($data['titre']);
    $article    = verifyInput($data['article']);
    $isSuccess  = true;

    if(empty($titre)){
        $titreError = "Vous devez rentrer un titre";
        $isSuccess = false;
    }

    if(empty($article)){
        $articleError = "Vous devez écrire un article";
        $isSuccess = false;
    }

    if($isSuccess){
        $bd = connect();
        try{
            $query = $bd->prepare("UPDATE blog SET titre = ?, article = ? WHERE blog_id = ?");
            $query->bindValue(1, $titre);
            $query->bindValue(2, $article);
            $query->bindValue(3, $_GET['blog_id']);
            $query-> execute();

        } catch (PDOException $e){
            $e->getMessage();
            die;
        }

            header("Location: cms.php");
    }
}