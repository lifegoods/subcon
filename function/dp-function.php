<?php

function renseigment(){
    $con = connect();
    try{
        $requete = $con->query("SELECT * from connexion");
        return $requete->fetch();
    } catch(PDOException $e){
        echo "Erreur : ".$e->getMessage();
    }
    $con = null;
    exit();
}

function article(){
    $con = connect();
    try{
        $requete = $con->query("SELECT * from blog");
        return $requete;
    } catch(PDOException $e){
        echo "Erreur : ".$e->getMessage();
    }
    $con = null;
    exit();
}

function info(){
    $con = connect();
    try{
        $requete = $con->query("SELECT * from infolettre");
        return $requete;
    } catch(PDOException $e){
        echo "Erreur : ".$e->getMessage();
    }
    $con = null;
    exit();
}

function produit(){
    $con = connect();
    try{
        $requete = $con->query("SELECT * from produit");
        return $requete;
    } catch(PDOException $e){
        echo "Erreur : ".$e->getMessage();
    }
    $con = null;
    exit();
}

function verifyInput($let){
    $let = trim($let);
    $let = stripslashes($let);
    $let = htmlspecialchars($let);
    return $let;
}