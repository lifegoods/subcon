<?php 

$titreError = $articleError = "";

if(!empty($_POST)){
    $titre      = verifyInput($_POST['titre']);
    $article    = verifyInput($_POST['article']);
    $isSuccess  = true;


    if(empty($titre)){
        $titreError = "Vous devez rentrer un titre";
        $isSuccess = false;
    }

    if(empty($article)){
        $articleError = "Vous devez écrire un article";
        $isSuccess = false;
    }
    if($isSuccess){
        
            $query = connect() -> prepare("INSERT INTO blog (titre, article, article_timestamp) VALUES (?, ?, ?)");
            $query ->bindValue(1, $titre);
            $query ->bindValue(2, $article);
            $query ->bindValue(3, time());

            $query-> execute();

            header("Location: cms.php");
    }
}