<?php 
    $user   = $password = "";
    $userError  = $passwordError = "";

    if (!isset($_SESSION["logged_in"])){
        if($_SERVER["REQUEST_METHOD"] == "POST"){

            $user       = verifyInput($_POST["username"]);
            print_r($user);
            $password   = verifyInput($_POST["userpassword"]);
            $isSuccess  = true;
            
            if(empty($password)){
                $mpError = "Vous devez rentrer un mot de passe!";
                $isSuccess = false;
            }

            if(empty($user)){
                $userError = "Vous devez rentrer un Nom d'utilisateur!";
                $isSuccess = false;
            } if($isSuccess){
                try{
                    $query = connect() -> prepare("SELECT * FROM connexion WHERE username = ? AND userpassword = ?");

                    $query ->bindValue(1, $user);
                    $query ->bindValue(2, $password);

                    $query ->execute();

                    $num = $query ->rowCount();
                }   catch (PDOException $e){
                    $e->getMessage();
                }

                if($num == 1){
                    $_SESSION["logged_in"] = true;
                    header("Location: cms.php");
                    exit();
                }
            }
        }
    }