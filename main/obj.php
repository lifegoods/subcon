<?php
session_start();
include_once $_SERVER["DOCUMENT_ROOT"]."/function/connecteur.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/function/dp-function.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/function/pre-function.php";


    $produit = produit();
    $produit = $produit->fetchAll();
?>


<body>
        <section class="wrapper">
            <div class="loginleBorder">
                <a href="SubCon.php">
                    <img src=img/subConLogo.svg alt="logo du Sub-Discombobulateur Atomique" class="logo2">
                </a>
                <h1 class="titreLogin">
                    Précommander le <br>Sub-discombobulateur Atomique
                </h1>
                <div class="formulaire">
                <form action="comfirmationPre.php" method="POST">
                    <input type="text" name="nom" class="champ" placeholder="Nom"><br>
                    <p class="comment"><?php echo $nomError; ?></p>
                    <input type="text" name="prenom" class="champ" placeholder="Prénom"><br>
                    <p class="comment"><?php echo $prenomError; ?></p>
                    <input type="email" name="email" class="champ" placeholder="Adresse Électronique"><br>
                    <p class="comment"><?php echo $emailError; ?></p>
                        <select class="select" name=prod onchange="this.form.submit();">
                            <?php foreach($produit as $produit) { ?>
                            <option value = "<?php echo $produit['id_produit']?>">
                                <?php echo $produit['produit']?>
                            </option>
                            <?php } ?>
                        </select>
                        <input type="submit" value="Précommander" class="button3">
                    </form>
                </div>
            </div>
        </section>
    </body>