<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/function/connecteur.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/function/dp-function.php";

?>


<body>
        <section class="wrapper">
            <div class="loginleBorder">
                <a href="SubCon.php">
                    <img src=img/subConLogo.svg alt="logo du Sub-Discombobulateur Atomique" class="logo2">
                </a>
                <div>
                <h1 class="titreLogin">Admin blog Sub-Discombobulateur</h1>
                <div class="list">
                    <ul>
                        <li>
                            <a href="add.php">
                                Ajouter une article
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="table">
                <table>
                        <tbody>
                        <?php
                            $query= article()->fetchAll();
                            foreach ($query as $row){
                        ?>
                            <tr>
                                <td>
                                    <?php echo $row["blog_id"]?>
                                </td>
                                <td>
                                    <?php echo $row["titre"]?>
                                </td>
                                <td>
                                    <a href="modifier.php?blog_id=<?php echo $row['blog_id']; ?>">
                                        Modifier
                                    </a>
                                </td>
                                <td><a href="supprimer.php?blog_id=<?php echo $row['blog_id']; ?>">
                                        Supprimer
                                    </a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                </table>
            </div>
        </section>
    </body>