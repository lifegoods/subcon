<?php
session_start();

include_once $_SERVER["DOCUMENT_ROOT"]."/function/connecteur.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/function/log-function.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/function/dp-function.php";

?>


<body>
        <section class="wrapper">
            <div class="loginleBorder">
                <a href="SubCon.php">
                    <img src=img/subConLogo.svg alt="logo du Sub-Discombobulateur Atomique" class="logo2">
                </a>
                <h1 class="titreLogin">
                    Donnez votre avis du
                    Sub-Discombobulateur Atomique <br>
                    avec <span class="blog">LE BLOG</span>
                </h1>
                <div class="formulaire">
                    <form action="<?php echo $page = "cms.php"; ?>" method="POST" name="login" autocomplete="off">
                        <input type="text" name="userName" class="champ" placeholder="Nom d'utilisateur"><br>
                            <?php if (isset($error)) { ?>
                                <p class="comment"><?php echo $userError; ?></p>
                            <?php } ?>
                        <input type="password" name="password" class="champ" placeholder="Mot de passe"><br>
                            <?php if (isset($error)) { ?>
                                <p class="comment"><?php echo $mpError; ?></p>
                            <?php } ?>
                        <input type="submit" value="Se connecter" class="button3">
                    </form>
                </div>
            </div>
        </section>
    </body>