<?php
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/connecteur.php";
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/dp-function.php";
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/edit-function.php";

if(!isset($_GET['blog_id'])){
    header('Location: /cms.php'  );
} else {
    $blog_id = $_GET['blog_id'];
}
?>


<body>
<section class="wrapper">
            <div class="loginleBorder">
                <a href="SubCon.php">
                    <img src=img/subConLogo.svg alt="logo du Sub-Discombobulateur Atomique" class="logo2">
                </a>
                <h1 class="titreLogin">
                   Modifier l'article
                </h1>
                <div class="formulaire">
                    <form action="<?php echo 'modifier.php?blog_id='.$blog_id ;?>" method="POST">
                        <input type="hidden" name="modifier">
                        <input type="text" name="titre" class="champ" placeholder="Titre"><br>
                        <p class="comment"><?php echo $titreError; ?></p>
                        <textarea row="200" cols="50" name="article" class="champ2" placeholder="Article"></textarea><br>
                        <p class="comment"><?php echo $articleError; ?></p>
                        <input type="submit" value="Publier" class="button3">
                    </form>
                </div>
            </div>
        </section>
    </body>