<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <title>Le Sub-Discombobulateur Atomique</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="icon" sizes="10x10" href="img/subConLogo.svg">
        <link href="https://fonts.googleapis.com/css2?family=Electrolize&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:700&display=swap" rel="stylesheet">
    </head>