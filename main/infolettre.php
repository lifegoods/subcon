<?php
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/connecteur.php";
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/dp-function.php";
include_once $_SERVER["DOCUMENT_ROOT"] ."/function/info-function.php";

?>
<section class="wrapper">
            <h1 class="titreInfo">
                Inscrivez-vous à l'infolettre <br>
                pour plus d'information sur ce <br>
                somptueux produit
            </h1>
            <div class="formulaire">
                <form action="comfirmation.php" method="POST">
                    <input type="text" name="nom" class="champ" placeholder="Nom"><br>
                    <p class="comment"><?php echo $nomError; ?></p>
                    <input type="text" name="prenom" class="champ" placeholder="Prénom"><br>
                    <p class="comment"><?php echo $prenomError; ?></p>
                    <input type="email" name="email" class="champ" placeholder="Adresse Électronique"><br>
                    <p class="comment"><?php echo $emailError; ?></p>
                    <input type="submit" value="S'inscrire à l'infolettre" class="button2">
                </form>
            </div>
        </section>