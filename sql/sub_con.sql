-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 02 août 2020 à 19:09
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sub_con`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `article` text NOT NULL,
  `article_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`blog_id`, `titre`, `article`, `article_timestamp`) VALUES
(1, 'JAJAJ', 'jAURÃ‰ DON DU', 1592354202),
(2, 'Raspatata', 'Rastpatata a aimÃ© le produit.', 1593021926),
(7, 'Teste', 'test 1 2 3', 1593046709),
(8, 'Teste', 'Teste', 1593050698),
(9, 'Article', 'Bla bla', 1593126733),
(10, 'Valentin', 'Valentin Ã©tait Ã  la plage. Il marchait sur la plage quand tout Ã  coup un doigt tomba du ciel!', 1595623718);

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

DROP TABLE IF EXISTS `connexion`;
CREATE TABLE IF NOT EXISTS `connexion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `connexion`
--

INSERT INTO `connexion` (`id`, `username`, `userpassword`) VALUES
(1, 'GussyPussy', 'flip123'),
(2, 'DustyBarry', 'flip123');

-- --------------------------------------------------------

--
-- Structure de la table `infolettre`
--

DROP TABLE IF EXISTS `infolettre`;
CREATE TABLE IF NOT EXISTS `infolettre` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`info_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `infolettre`
--

INSERT INTO `infolettre` (`info_id`, `nom`, `prenom`, `email`) VALUES
(1, 'Pussy', 'Gussy', 'gussypussyboy@hotmail.com'),
(2, 'Louis', 'Garneau', 'louis_garneau@hotmail.fr'),
(8, 'Tremblay', 'Ke$ha', 'chatroyal2@caramail.com'),
(7, 'Gaumont', 'Jean-Luc', 'jlgomoncave@hotmail.com'),
(11, 'Brillant', 'Jimmy', 'lolypop@hotmail.com'),
(10, 'Bouchard', 'John', 'jojo@hotmail.com'),
(20, 'Bernatchez', 'Eliot', 'EliBer@hotmail.com'),
(14, 'Boisbrillant', 'RÃ©mie', 'brilleremi@sympatico.ca');

-- --------------------------------------------------------

--
-- Structure de la table `precommande`
--

DROP TABLE IF EXISTS `precommande`;
CREATE TABLE IF NOT EXISTS `precommande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `precommande`
--

INSERT INTO `precommande` (`id`, `nom`, `prenom`, `email`) VALUES
(1, 'Pussy', 'Gussy', 'GussyPussyBoy@hotmail.com'),
(2, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(3, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(4, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(5, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(6, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(7, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(8, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(9, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(10, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com'),
(11, 'Lavoie', 'Alexandre', 'AleLa@hotmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id_produit` int(11) NOT NULL AUTO_INCREMENT,
  `produit` varchar(255) NOT NULL,
  PRIMARY KEY (`id_produit`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `produit`) VALUES
(1, 'La version de base'),
(2, 'La version de pas base'),
(3, 'La version de gros gros luxe');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
